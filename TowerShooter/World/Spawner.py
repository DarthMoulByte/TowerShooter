from bgez.framework.types import GameObject
from bgez.framework import events
from bgez import utils

import random
import bgez

__all__ = ['Spawner']

class Spawner(GameObject):
    LINK = 'Spawner.000'

    REGISTRY = utils.List()

    @classmethod
    async def RandomSpawn(cls, enemies, level=0, delay=1):
        spawner = random.choice(cls.REGISTRY)
        for enemy in enemies:
            await events.skip(seconds=delay)

            e = spawner.spawn(enemy)
            e.MAX_HEALTH *= 1 + level / 2
            e.HEALTH = e.MAX_HEALTH

    def construct(self):
        self.REGISTRY.append(self)

    def spawn(self, enemy):
        return self.scene.addAsset(enemy, reference=self)