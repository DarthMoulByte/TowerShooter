from bgez.framework.types import GameObject
from bgez import utils

from TowerShooter.Entities import Enemy
from TowerShooter.Entities import Damageable

import bgez

__all__ = ['Nexus']

class Nexus(GameObject, Damageable):
    LINK = 'Nexus.000'
    REGISTRY = utils.List()

    MAX_HEALTH = 100

    def construct(self):
        self.health = self.MAX_HEALTH
        self.REGISTRY.append(self)

    def hit(self, attacker):
        if not isinstance(attacker, Enemy.Enemy):
            return False
        else:
            self.health -= 1
            self.health = max(self.health, 0)
            return True