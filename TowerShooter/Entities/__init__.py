from bgez.framework.types import GameObject
import bgez

class Collider(GameObject):
    REGISTER = True

    def construct(self, Hitbox):
        self.hitbox = Hitbox
        self.objects_hit = set()
        self.__collisions = [None]

        #A 4 items list: 
        #[0] is the function wich returned value is used to stop the collisions detection
        #[1] are the function's arguments
        #[2] are the function's keyworded arguments
        #[3] is the value function must yield to stop the collisions detection
        self.__stop_func = []

        @self.hitbox.collisionCallbacks.append
        def collision(object, position, normal):
            if object not in self.objects_hit and self.__stop_func:
                self.objects_hit.add(object)
                self.on_collision(object, position, normal)

            frame = bgez.logic.getFrameTime()
            if self.__collisions[0] != frame:
                self.__collisions[1:] = ()
                self.__collisions[0] = frame
            self.__collisions.append(
                (object, position, normal)
            )

    def get_current_collisions(self):
        if self.__collisions[0] == bgez.logic.getFrameTime():
            return self.__collisions[1:]
        return ()

    #Used to start collsions detection during an unknown amount of time.
    #Keep on detecting new collisions until stop_func returned value (see post_draw).
    def start_detection(self, stop_func, *args, value=True, **kargs):
        self.objects_hit.clear()
        self.__stop_func.extend((stop_func, args, kargs, value))

    def on_collision(self, obj, pos, normal):
        #to be override by children. Is executed on every new collision
        #after start_detection
        pass

    def post_draw(self, scene):
        if self.__stop_func:
            value = self.__stop_func[0](*self.__stop_func[1],**self.__stop_func[2])
            if value == self.__stop_func[3]:
                self.__stop_func.clear()

class Damageable(object):

    def hit(self, attacker):
        return False

class Deployable(object):

    BUILDABLE = 'buildable'

class Usable(object):

    pass
