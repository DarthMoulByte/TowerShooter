
import bgui
import bgui.bge_utils

__all__ = ['HUD']

class HUD(bgui.bge_utils.Layout):

    def __init__(self, sys, data):
        super().__init__(sys, data)

        #Main frame, holds all widgets. Pos is from bottom left corner
        self.win = bgui.Frame(
            self,
            border=0,
            size=[1, 1],
            options=
                bgui.BGUI_DEFAULT |
                bgui.BGUI_CENTERED
        )

        self.lbl_gold = bgui.Label(
            self.win,
            text="golds",
            pos=[0.05, 0.05],
            sub_theme='Gold',
            options = bgui.BGUI_DEFAULT
        )

        self.nexus_health = bgui.ProgressBar(
            self.win,
            percent=1.0,
            size=[0.6, 0.08], 
            pos=[0.0, 0.9],
            sub_theme="Nexus", 
            options=
                bgui.BGUI_DEFAULT |
                bgui.BGUI_CENTERX
        )

        self.lbl_main = bgui.Label(
            self.win,
            text="",
            pos=[0.05, 0.05],
            sub_theme='Main',
            options = 
                bgui.BGUI_DEFAULT |
                bgui.BGUI_CENTERX |
                bgui.BGUI_CENTERY
        )

        self.lbl_ammo = bgui.Label(
            self.win,
            text="",
            pos=[0.85, 0.05],
            options = 
                bgui.BGUI_DEFAULT
        )

        self.player_health = bgui.ProgressBar(
            self.win,
            percent=1.0,
            size=[0.1, 0.04], 
            pos=[0.05, 0.15],
            sub_theme="Health", 
            options=
                bgui.BGUI_DEFAULT
        )